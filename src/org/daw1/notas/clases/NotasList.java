/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.notas.clases;

import java.util.ArrayList;
import java.util.Objects;
import com.google.common.base.Preconditions;
import java.util.Collection;
/**
 *
 * @author rgcenteno
 */
public class NotasList extends ArrayList<Integer> implements INumericList{
    
    public NotasList() {
        super();
    }
    
    public NotasList(java.util.Collection<Integer> c){
        super(c);
    }        
    
    @Override
    public boolean add(Integer e) {
        checkNota(e);
        return super.add(e);
    }

    
    @Override
    public void add(int index, Integer element) {
        checkNota(element);
        super.add(index, element);         
    }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        checkNota(c);        
        return super.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {        
        checkNota(c);           
        return super.addAll(index, c);            
    }

    @Override
    public Integer set(int index, Integer element) {
        checkNota(element);                
        return super.set(index, element);               
    }

    @Override
    public Integer remove(int index) {
        return super.remove(index); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void checkNota(Integer e){
        Objects.requireNonNull(e);
        Preconditions.checkArgument(e >= 0 && e <= 10, "La nota sólo puede tener un valor comprendido entre 0 y 10", e);        
    }
    
    private void checkNota(Collection<? extends Integer> c){
        for(Integer e: c){
            checkNota(e);      
        }
    }
    
    public double getMedia(){
        if(this.isEmpty()){
            return -1;
        }
        else{
            double media = 0;
            for(Integer nota : this){  
                media += nota;
            }
            return media / this.size();
        }
    }
    
    public int getMaxNota(){
        if(this.isEmpty()){
            return -1;
        }
        else{
            int max = 0;
            for(Integer nota : this){  
                if(max < nota){
                    max = nota;
                }
            }
            return max;
        }
    }
    
    public int getMinNota(){
        if(this.isEmpty()){
            return -1;
        }
        else{
            int min = 10;
            for(Integer nota : this){  
                if(min > nota){
                    min = nota;
                }
            }
            return min;
        }
    }
    
    public boolean isTodoAprobado(){
        for(Integer nota : this){            
            if(nota < 5){
                return false;
            }
        }
        return true;
    }
    
    public boolean haySuspenso(){
        return !isTodoAprobado();
    }
    
    public double porcentajeAprobados(){
        int contador = 0;
        for(Integer nota : this){
            if(nota >= 5){
                contador++;
            }
        }
        return !this.isEmpty() ? (double)contador / this.size() : -1;
    }
    
    public double porcentajeSuspensos(){
        int contador = 0;
        for(Integer nota : this){
            if(nota < 5){
                contador++;
            }
        }
        return !this.isEmpty() ? (double)contador / this.size() : -1;
    }
}
