/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.notas.clases;

/**
 *
 * @author rgcenteno
 */
public interface INumericList extends java.util.List<Integer>{
    public double getMedia();
    public int getMaxNota();
    public int getMinNota();
    public boolean isTodoAprobado();
    public boolean haySuspenso();
    public double porcentajeSuspensos();
    public double porcentajeAprobados();
}
