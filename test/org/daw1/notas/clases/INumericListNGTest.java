/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.daw1.notas.clases;

import org.junit.jupiter.api.BeforeAll;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.HashMap;

/**
 *
 * @author rgcenteno
 */
public class INumericListNGTest {
    
    private static INumericList lista;
    private static INumericList listaAprobados;
    private static INumericList listaSuspensos;
    
    private static java.util.Map<String, INumericList> listas;
    
    private static java.util.Map<String, java.util.Map<String, Number>> expected;
        
    @BeforeAll
    public static void setUpClass() {        
        
        
    }
    
    @BeforeClass
    public static void setUpBeforeClass(){
        expected = new java.util.HashMap<>();
        expected.put("testGetMedia", new HashMap<>());
        expected.get("testGetMedia").put("lista", 5.5);
        expected.get("testGetMedia").put("listaAprobados", 7.1);
        expected.get("testGetMedia").put("listaSuspensos", 2.0);
        
        expected.put("testGetMaxNota", new HashMap<>());
        expected.get("testGetMaxNota").put("lista", 10);
        expected.get("testGetMaxNota").put("listaAprobados", 10);
        expected.get("testGetMaxNota").put("listaSuspensos", 4);
        
        expected.put("testGetMinNota", new HashMap<>());
        expected.get("testGetMinNota").put("lista", 1);
        expected.get("testGetMinNota").put("listaAprobados", 5);
        expected.get("testGetMinNota").put("listaSuspensos", 0);
        
        expected.put("testPorcentajeSuspensos", new HashMap<>());
        expected.get("testPorcentajeSuspensos").put("lista", 0.4);
        expected.get("testPorcentajeSuspensos").put("listaAprobados", 0d);
        expected.get("testPorcentajeSuspensos").put("listaSuspensos", 1d);
        
        expected.put("testPorcentajeAprobados", new HashMap<>());
        expected.get("testPorcentajeAprobados").put("lista", 0.6);
        expected.get("testPorcentajeAprobados").put("listaAprobados", 1d);
        expected.get("testPorcentajeAprobados").put("listaSuspensos", 0d);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
        lista = new NotasList();
        listaAprobados = new NotasList();
        listaSuspensos = new NotasList();
        for(int i = 0; i < 10; i++){
            lista.add(i+1);
            listaAprobados.add((i % 6) + 5);
            listaSuspensos.add((i % 5));
        }
        listas = new java.util.TreeMap<>();
        listas.put("lista", lista);
        listas.put("listaAprobados", listaAprobados);
        listas.put("listaSuspensos", listaSuspensos);
        for(INumericList l : listas.values()){
            System.out.println(l);
        }
        
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getMedia method, of class INumericList.
     */
    @Test
    public void testGetMedia() {
        System.out.println("getMedia");
        for(java.util.Map.Entry<String, INumericList> entry : listas.entrySet()){
            INumericList instance = entry.getValue();
            double expResult = (Double)expected.get("testGetMedia").get(entry.getKey());
            double result = instance.getMedia();
            assertEquals(result, expResult, entry.getKey() + ": " + instance);
        }
    }

    /**
     * Test of getMaxNota method, of class INumericList.
     */
    @Test
    public void testGetMaxNota() {
        System.out.println("getMaxNota");
        for(java.util.Map.Entry<String, INumericList> entry : listas.entrySet()){
            INumericList instance = entry.getValue();
            int expResult = (Integer)expected.get("testGetMaxNota").get(entry.getKey());
            int result = instance.getMaxNota();
            assertEquals(result, expResult, entry.getKey() + ": " + instance);
        }
    }

    /**
     * Test of getMinNota method, of class INumericList.
     */
    @Test
    public void testGetMinNota() {
        System.out.println("getMinNota");
        for(java.util.Map.Entry<String, INumericList> entry : listas.entrySet()){
            INumericList instance = entry.getValue();
            int expResult = (Integer)expected.get("testGetMinNota").get(entry.getKey());
            int result = instance.getMinNota();
            assertEquals(result, expResult, entry.getKey() + ": " + instance);
        }
    }

    /**
     * Test of isTodoAprobado method, of class INumericList.
     */
    @Test
    public void testIsTodoAprobado() {
        System.out.println("isTodoAprobado");
        for(java.util.Map.Entry<String, INumericList> entry : listas.entrySet()){
            INumericList instance = entry.getValue();
            boolean expResult = entry.getKey().equals("listaAprobados");
            boolean result = instance.isTodoAprobado();
            assertEquals(result, expResult, entry.getKey() + ": " + instance);
        }
    }

    /**
     * Test of haySuspenso method, of class INumericList.
     */
    @Test
    public void testHaySuspenso() {
        System.out.println("haySuspenso");
        for(java.util.Map.Entry<String, INumericList> entry : listas.entrySet()){
            INumericList instance = entry.getValue();
            boolean expResult = !(entry.getKey().equals("listaAprobados"));
            boolean result = instance.haySuspenso();
            assertEquals(result, expResult, entry.getKey() + ": " + instance);
        }
    }
        
    @Test
    public void testPorcentajeSuspensos() {
        System.out.println("porcentajeSuspensos");
        for(java.util.Map.Entry<String, INumericList> entry : listas.entrySet()){
            INumericList instance = entry.getValue();
            double expResult = (Double)expected.get("testPorcentajeSuspensos").get(entry.getKey());
            double result = instance.porcentajeSuspensos();
            assertEquals(result, expResult, entry.getKey() + ": " + instance);
        }
    }
    
    /**
     * Test of haySuspenso method, of class INumericList.
     */
    @Test
    public void testPorcentajeAprobados() {
        System.out.println("porcentajeAprobados");
        for(java.util.Map.Entry<String, INumericList> entry : listas.entrySet()){
            INumericList instance = entry.getValue();
            double expResult = (Double)expected.get("testPorcentajeAprobados").get(entry.getKey());
            double result = instance.porcentajeAprobados();
            assertEquals(result, expResult, entry.getKey() + ": " + instance);
        }
    }
    
}
