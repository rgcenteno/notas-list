Estamos realizando una aplicación de gestión de alumnado y nos damos cuenta de que nos estamos encontrando con una problemática muy habitual. Los alumnos tienen un conjunto de notas que tienen que ser un entero entre 0 y 10. Las materias también tienen un listado de notas, así como los informes de Ciclo y demás funcionalidades.

Vemos que cada vez que queremos añadir una nota a una clase tenemos que hacer siempre las mismas operaciones de check y que para obtener medias, valores máximos o valores mínimos tenemos que repetir código en todas las clases.

Para solucionar esta problemática hemos decidido crear una subclase llamada lista de notas que extenderá de ArrayList. Dicha clase tendrá los mismos constructores que ArrayList pero comprobando que los valores que se reciben son correctos. Además sobreescribiremos todos los métodos que insertan notas para comprobar que se inserta un valor válido (entre 0 y 10).

Aprovecharemos que tenemos esta clase para implementar las siguientes funcionalidades:

    getMedia(): Devuelve -1 si no hay notas. En caso contrario devuelve la media de las notas.
    getMax(): Devuelve -1 si no hay notas. Si no devuelve la nota más alta.
    getMin(): Devuelve -1 si no hay notas. Si no devuelve la nota más baja.
    isTodoAprobado(): Devuelve true si todas las notas son >= 5
    haySuspenso(): Devuelve true si al menos una nota es < 5
    porcentajeAprobados(): Devuelve el porcentaje de notas aprobadas. Si no hay notas devuelve -1.
    porcentajeSuspensos(): Devuelve el porcentaje de notas suspensas. Si no hay notas devuelve -1.

Una vez tenemos realizado esta clase vamos a probar su utilidad:

Crea una clase alumno con nombre, número expediente y notas. Agrega un método que permita añadir una nota, otro que permita añadir varias notas y por último un método que permita saber la media y otro que permita saber si el alumno ha aprobado todo.

Ahora crea una clase Módulo que contenga alumnos. haz que el módulo pueda devolver la media de la clase, el porcentaje de aprobados y el porcentaje de suspensos.
